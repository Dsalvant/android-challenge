# Android developer challenge


### Task
Spend 24 hours maximum from the moment you specify your start date and time.
Typically the task should take about 2 or 3 hours of work.

Create an android app that will allow to search for movies using themoviedb API - https://developers.themoviedb.org/3/search/search-movies

1. The app should contain a search bar at the very top
2. When a user enters the search query, the app should make a call to the API and display the results
3. Results should appear automatically - without pressing Enter or Search buttons
4. Make sure you display loading indicators and handle errors properly


### Important notes
* You’ll be provided with the skeleton project and a  themoviedb API key. Feel free to change the project in order to meet requirements listed below.
* Application should support api v21 and above
* Application must be applicable for high performance standards(low memory usage, ~60 fps, low battery usage)
* Application must compile and run with no crashes.


### What we expect from the code challenge
* Good software engineering practices are used
* The code follows SOLID principles
* MVP/MVI/MVVM architecture pattern is used
* The app’s UI is build as close to the provided mocks as possible


### Resources
1. TheMovideDB API key - c058d9a291e7f1dd69f97f1afac69b61
2. Skeleton project - this repository
3. Mock - https://www.figma.com/file/8o34ycBGrbCe7FkMz9D0Wr/Squire-Android-challenge-mocks?node-id=0%3A1


### Deliverables 
None. Be prepared to walk me through all steps in technical detail over zoom via 
screen share.