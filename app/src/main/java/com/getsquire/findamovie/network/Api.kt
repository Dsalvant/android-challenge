package com.getsquire.findamovie.network

import com.getsquire.findamovie.network.response.SearchMovieResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("search/movies")
    fun searchMovie(
        @Query("api_key") apiKey: String,
        @Query("query") query: String,
        @Query("page") page: Int = 1
    ): Single<SearchMovieResponse>
}